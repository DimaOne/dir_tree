package main

import (
	"fmt"
	"io"
	"os"
	// "path/filepath"
	"sort"
	"strings"
)

var tab = map[string]string{
	"BeginEmpty": "\t",
	"BeginContinue": "│\t",
	"FinishMiddle": "├───",
	"FinishLast":   "└───",
}

func size(info *os.FileInfo) string {
	if (*info).Size() == 0 {
		return "(empty)"
	}

	return fmt.Sprintf("(%vb)", (*info).Size())
}

func ClearTabs(tabs []string) string {
	tempTabs := make([]string, len(tabs))
	copy(tempTabs, tabs)
	if len(tempTabs) > 1 {
		for i, v := range tempTabs[:len(tempTabs)-1] {
			if v == tab["FinishLast"] {
				tempTabs[i] = tab["BeginEmpty"]
			} else if v == tab["FinishMiddle"] {
				tempTabs[i] = tab["BeginContinue"]
			}
		}
	}
	return strings.Join(tempTabs, "")
}

func reqTree(out io.Writer, path string, printFiles bool, tabs []string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()
	info, _ := file.Stat()
	if info.IsDir() {
		list, _ := file.Readdirnames(0)
		if printFiles == false {
			listTmp := make([]string, 0, len(list))
			for _, fi := range list {
				fileTmp, _ := os.Open(fmt.Sprintf("%v/%v", path, fi))
				infoTmp, _ := fileTmp.Stat()
				if infoTmp.IsDir() {
					listTmp = append(listTmp, fi)
				}
			}
			list = listTmp
		}

		sort.Strings(list)
		if len(tabs) != 0 {
			io.WriteString(out, fmt.Sprintf("%v%v\n", ClearTabs(tabs), info.Name()))
		}
		tabs = append(tabs, tab["FinishMiddle"])
		for i, fi := range list {
			if i == len(list)-1 {
				tabs[len(tabs)-1] = tab["FinishLast"]
			}
			reqTree(out, fmt.Sprintf("%v/%v", path, fi), printFiles, tabs)
		}
	} else {
		if printFiles {
			io.WriteString(out, fmt.Sprintf("%v%v %v\n", ClearTabs(tabs), info.Name(), size(&info)))
		}
	}
	return nil
}

func dirTree(out io.Writer, path string, printFiles bool) error {
	tabs := make([]string, 0, 16)
	return reqTree(out, path, printFiles, tabs)
}

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}
